import React from "react";

export default function Aboutus() {
  return (
    <div>
      {" "}
      <section className="flex flex-col items-center p-10">
        <div className="container">
          <div className="flex flex-row justify-center m-10 px-10 ">
            <div className="grid grid-cols-1 gap-4 md:grid-cols-2 ">
              <div className="flex basis-1/2 box-border p-4 text-center">
                <img
                  className="object-cover h-full w-full"
                  src="image/Technology_Home.jpg"
                  alt="/"
                />
              </div>
              <div className="flex flex-col box-border">
                <p className="text-sm text-cyan-600 font-bold">ABOUT US</p>
                <h1 className="text-xl font-bold mt-4">
                  Uplift Your Digital Life
                </h1>
                <p className="text-xs mt-4">
                  เทคโนโลยีไม่เคยเป็นศัตรูของใคร ด้วยการพัฒนาทรัพยากร
                  และแนวทางการใช้ทรัพยากรที่เหมาะสม
                  มันสามารถเปลี่ยนธุรกิจและชีวิตของคุณไปตลอดกาล
                </p>
                <p className="text-xs mt-3">
                  ด้วยประสบการณ์กว่า 25 ปี MFEC จึงพร้อมที่จะเป็น Technical
                  Partner และ Transform ธุรกิจของคุณสู่การเป็น Technology-Driven
                  Enterprise ด้วยทรัพยากรและการให้บริการที่ดีที่สุด ให้ MFEC
                  เป็นส่วนหนึ่งในการพาธุรกิจของคุณไปสู่ความสำเร็จ
                  และยกระดับคุณภาพชีวิตดิจิทัลของสังคมไปพร้อม ๆ กัน
                </p>
                <a href="https://www.mfec.co.th/about/" className="mt-2">
                  <button className="bg-cyan-600 font-sans text-white">
                    Know us better &rarr;{" "}
                  </button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
