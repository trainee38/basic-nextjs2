import React from 'react'
import styles from "../styles/Home.module.css";
import { BiGrid } from "react-icons/bi";
import { BiData } from "react-icons/bi";
import { FaUserLock } from "react-icons/fa";
import { BiCloud } from "react-icons/bi";
import { RiLightbulbLine } from "react-icons/ri";


export default function Business() {
  return (
    <main className={styles.main}>
        <section className="flex items-center justify-center p-10">
          <div className="flex flex-row flex-wraps bg-blue-200  ">
            <div className="flex-col grid grid-row-2  px-10">
              <div className="flex-initial content-center pt-20 pb-10 text-6xl text-bold text-blue-900">
                <div className="flex-initial pb-5 text-2xl text-bold text-blue-900">
                  services
                </div>
                <div className="break-words ">Uplift Your</div>
                <div className="break-normal ">Business greater.</div>
                <div className="button ">
                  <button
                    type="button"
                    className="text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                  >
                    Read more
                  </button>
                </div>
              </div>
            </div>
            <div className="grid grid-cols-2 pb-10 pt-10 md:grid-flow-row bg-cyan-50 content-around">
              <div className="flex flex-row pb-10 text-xl text-bold border border-whtie-500">
                <BiGrid></BiGrid>
                <div className="break-normal px-5">
                  <p className="font-bold">Application</p>
                  <p> สร้างความเป็นไปได้ทางธุรกิจด้วย</p>
                  <p>บริการที่ครบวงจร ตั้งแต่ Software</p>
                  <p>Development Life Cycle ไปจนถึง</p>
                  <p>Agile Development</p>
                </div>
              </div>

              <div className="flex flex-row pb-10 text-xl text-bold  border border-whtie-500">
                <BiData></BiData>
                <div className="break-normal px-5">
                  <p className="font-bold">Big Data & Analytics</p>
                  <p> นำข้อมูลมาใช้อย่างมีประสิทธิภาพ </p>
                  <p>พร้อมเปลี่ยนทุกรายละเอียดให้เป็น</p>
                  <p>ข้อมูลเชิงลึกที่มีคุณค่า</p>
                </div>
              </div>

              <div className="flex flex-row pb-10 text-xl text-bold  border border-whtie-500">
                <FaUserLock></FaUserLock>
                <div className="break-normal px-5">
                  <p className="font-bold">Cyber Security</p>
                  <p> อัปเกรดการรักษาความปลอดภัย</p>
                  <p>ของธุรกิจคุณในยุคดิจิทัล รวมถึง </p>
                  <p>ความปลอดภัยของทรัพย์สินดิจิทัลต่าง ๆ ด้วยโซลูชันการ</p>
                  <p>รักษาความปลอดภัยทางไซเบอร์ที่คัดสรรมาเพื่อคุณ</p>
                </div>
              </div>

              <div className="flex flex-row pb-10 text-xl text-bold  border border-whtie-500">
                <BiCloud></BiCloud>
                <div className="break-normal px-5">
                  <p className="font-bold">Cloud Platform</p>
                  <p> เข้าถึงทุกข้อมูลทุกที่ ทุกเวลา</p>
                  <p> ตลอด 24 ชั่วโมง ผ่านระบบคลาวด์ </p>
                </div>
              </div>

              <div className="flex flex-row pb-10 text-xl text-bold border border-whtie-500">
                <RiLightbulbLine></RiLightbulbLine>
                <div className="break-normal px-5">
                  <p className="font-bold">Enterprise IT Solution</p>
                  <p> เร่งความเร็วให้กับ Digital  </p>
                  <p>Transfromation ในยุค </p>
                  <p>Disruption ด้วยนวัตกรรมสำหรับองค์กรที่เหนือกว่า</p>
                </div>
              </div>

              <div className="flex flex-row pb-10 text-xl text-bold border border-whtie-500">
                <BiGrid></BiGrid>
                <div className="break-normal px-5">
                  <p className="font-bold">Outsource Services</p>
                  <p> เพิ่มความแข็งแกร่งให้กับทีมงาน IT Infrastructure </p>
                  <p>ด้วย IT Operation Managed Services (ITOMS)</p>

                </div>
              </div>
            </div>
          </div>

        </section>
      </main>
  )
}
