import React from "react";
import styles from "../styles/Home.module.css";

export default function Main3() {
  return (
    <div>
       <section className="flex items-center justify-center p-10">
      <div className="container">
        <div className="flex grid grid-rows-3 gap-1 justify-center m-10">
          <div className="grid justify-center">
            <h1 className="text-xl font-mono font-bold">Industry-Wise Technology</h1>
          </div>
          <div className={styles.wrapper}>
            <div className={styles.words}>
              <span>Banking/Finance</span>
              <span>Government</span>
              <span>Telecommunication</span>
              <span>Energy/Utility</span>
            </div>
          </div>
          <div className="grid justify-center">
            <h1 className="text-sm text-gray-400 font-mono">MFEC ให้บริการทางด้านเทคโนโลยีครอบคลุมหลากหลายอุตสาหกรรม</h1>
          </div>
        </div>
      </div>

    </section>

      <section>
        <div className="flex items-center justify-center p-10">
          <div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            <div className="group relative cursor-pointer items-center justify-center overflow-hidden hover:shadow-black/30">
              <div className="h-96 w-72">
                <img
                  className="h-full w-full object-cover transition-transform duration-500 group-hover:scale-110"
                  src="https://www.mfec.co.th/wp-content/uploads/2022/05/Home_Banking.jpg"
                  alt=""
                />
              </div>
              <div className="absolute inset-0 bg-gradient-to-b from-transparent via-transparent to-black group-hover:via-black/60 group-hover:to-black/70"></div>
              <div className="absolute inset-0 flex translate-y-[50%] flex-col items-center justify-center px-9 text-center duration-500 group-hover:translate-y-0">
                <h1 className="font-dmserif text-lg font-bold text-white">
                  Banking/Finance
                </h1>
                <p className="mb-3 text-sm text-white opacity-0 transition-opacity duration-300 group-hover:opacity-100 mt-5">
                  นโยบายทางการเงินและพฤติกรรมการใช้จ่ายของผู้บริโภคเปลี่ยนแปลงอยู่ตลอดเวลา
                  ดังนั้นเราจะทำให้เทคโนโลยีของคุณไม่ตกเทรนด์
                </p>
              </div>
            </div>
            <div className="group relative cursor-pointer items-center justify-center overflow-hidden hover:shadow-black/30">
              <div className="h-96 w-72">
                <img
                  className="h-full w-full object-cover transition-transform duration-500 group-hover:scale-110"
                  src="https://www.mfec.co.th/wp-content/uploads/2022/05/Home_Government.jpg"
                  alt=""
                />
              </div>
              <div className="absolute inset-0 bg-gradient-to-b from-transparent via-transparent to-black group-hover:via-black/60 group-hover:to-black/70"></div>
              <div className="absolute inset-0 flex translate-y-[50%] flex-col items-center justify-center px-9 text-center duration-500 group-hover:translate-y-0">
                <h1 className="font-dmserif text-lg font-bold text-white">
                  Government
                </h1>
                <p className="mb-3 text-sm text-white opacity-0 transition-opacity duration-300 group-hover:opacity-100  mt-5">
                  เราช่วยยกระดับความก้าวหน้าทางด้านสังคมในยุคดิจิทัลด้วยเทคโนโลยีที่เชื่อถือได้และตอบโจทย์กับความต้องการของผู้คน
                </p>
              </div>
            </div>
            <div className="group relative cursor-pointer items-center justify-center overflow-hidden hover:shadow-black/30">
              <div className="h-96 w-72">
                <img
                  className="h-full w-full object-cover transition-transform duration-500 group-hover:scale-110"
                  src="https://www.mfec.co.th/wp-content/uploads/2022/05/Telecommunication_image.jpg"
                  alt=""
                />
              </div>
              <div className="absolute inset-0 bg-gradient-to-b from-transparent via-transparent to-black  group-hover:via-black/60 group-hover:to-black/70"></div>
              <div className="absolute inset-0 flex translate-y-[50%] flex-col items-center justify-center px-9 text-center duration-500 group-hover:translate-y-0">
                <h1 className="font-dmserif text-lg font-bold text-white">
                  Telecommunication
                </h1>
                <p className="mb-3 text-sm text-white opacity-0 transition-opacity duration-300 group-hover:opacity-100  mt-5">
                  เทคโนโลยีที่ก้าวหน้าเป็นตัวเชื่อมให้ผู้คนใกล้ชิดกันมากยิ่งขึ้นไม่ว่าจะอยู่มุมไหนของโลกก็ตาม
                </p>
              </div>
            </div>
            <div className="group relative cursor-pointer items-center justify-center overflow-hidden hover:shadow-black/30">
              <div className="h-96 w-72">
                <img
                  className="h-full w-full object-cover transition-transform duration-500 group-hover:scale-110"
                  src="https://www.mfec.co.th/wp-content/uploads/2022/05/Governament_image.jpg"
                  alt=""
                />
              </div>
              <div className="absolute inset-0 bg-gradient-to-b from-transparent via-transparent to-black group-hover:via-black/60 group-hover:to-black/70"></div>
              <div className="absolute inset-0 flex translate-y-[55%] flex-col items-center justify-center px-9 text-center duration-500 group-hover:translate-y-0">
                <h1 className="font-dmserif text-lg font-bold text-white">
                  Energy/Utility
                </h1>
                <p className="mb-3 text-sm text-white opacity-0 transition-opacity duration-300 group-hover:opacity-100 mt-5">
                  เพิ่มความสามารถในการตอบโจทย์ความต้องการของอุตสาหกรรมพลังงานและสาธารณูประโภค
                  ในขณะเดียวกันก็ยังลดผลกระทบต่อสิ่งแวดล้อม
                  สิ่งเหล่านี้ทำได้ด้วยการพัฒนาเทคโนโลยีที่ตรงประเด็น
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
