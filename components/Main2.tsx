import React from 'react'
import styles from "../styles/Home.module.css";

export default function Main2() {
  return (
    <section className='p-20'>
      <div className='flex justify-center w-full'>
        <div className='flex items-center flex-col'>
          <div className={`mb-5 ${styles.textTitle2}`}>
            <h6> OUR PARTNERS </h6>
          </div>
          <div className={`mb-4 ${styles.textSm}`}>
            <p> Impact cannot be created without PARTNERS</p>
          </div>
          <div>
            <div className='flex items-center flex-row'>
              <div>
                <img className='w-[160px] h-[60px] p-1' src="https://www.mfec.co.th/wp-content/uploads/2022/02/Oracle-gold-300x99.png" alt="mfec" />
              </div>
              <div>
              <img className='w-[150px] h-[77px] p-1' src="https://www.mfec.co.th/wp-content/uploads/2022/02/Microsoft-gold-300x153.png" alt="mfec" />
              </div>
              <div>
              <img className='w-[150px] h-[80px] p-1' src="https://www.mfec.co.th/wp-content/uploads/2022/02/google-logo-150x150.png" alt="mfec" />
              </div>
              <div>
              <img className='w-[150px] h-[75px] p-1' src="https://www.mfec.co.th/wp-content/uploads/2022/02/IBM_Gold-BP_Mark_Pos_RGB-300x150.jpg" alt="mfec" />
              </div>
              <div>
              <img className='w-[100px] h-[87px] p-1' src="https://www.mfec.co.th/wp-content/uploads/2022/02/partner-logo-300x269.png" alt="mfec" />
              </div>
              <div>
              <img className='w-[80px] h-[50px] p-1' src="https://www.mfec.co.th/wp-content/uploads/2022/02/AWS_logo.png" alt="mfec" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  )
}
