import React from "react";
import styles from "../styles/Home.module.css";

export default function Main1() {
  return (
    <section className={`p-5 flex justify-center w-full`}>
      <div className="flex flex-col flex-wrap">
        <div className="flex flex-col p-5">
          <div className={styles.textTitle1}>Awards and Testimonials</div>
          <div className={styles.textMain1}>
            <p>Stories Speak Louder</p>
          </div>
          <div className={styles.textSm}>
            <p>
              ตอกย้ำความสำเร็จด้านประสิทธิภาพการทำงานและความไว้วางใจตลอดมา MFEC
              มุ่งมั่นที่จะสร้างสรรค์และพัฒนาคุณภาพต่อไป
            </p>
          </div>
        </div>

        <div className="flex justify-center flex-row mr-20">
          <div className="flex flex-col p-5">
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="rounded-t-lg"
                  src="https://www.mfec.co.th/wp-content/uploads/2022/09/Kong_Winnner_Partner_for_Web-02-640x364.jpg"
                  alt=""
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                  MFEC named Kong Partner of the Year 2022
                  </h5>
                </a>
              </div>
            </div>
          </div>
          <div className="flex flex-col p-5">
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="rounded-t-lg"
                  src="https://www.mfec.co.th/wp-content/uploads/2022/09/Veeam_Rising_Star_for_Web-01-640x364.jpg"
                  alt=""
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 mt-[33px] text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    MFEC คว้า 2 รางวัลจาก Veeam

                  </h5>
                </a>
              </div>
            </div>
          </div>
          <div className="flex flex-col p-5">
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="rounded-t-lg"
                  src="https://www.mfec.co.th/wp-content/uploads/2022/09/F5_Award-01-640x364.jpg"
                  alt=""
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                  MFEC คว้ารางวัล Top Security Contribution Award 2022
                  </h5>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col">
          <div className="mt-3 ml-[1050px] flex justify-center">
            <p className={styles.readmore}>Read more &gt; </p>
          </div>
        </div>
      </div>
    </section>
  );
}
