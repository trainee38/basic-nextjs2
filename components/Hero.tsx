import React from "react";
import styles from "../styles/Home.module.css";
import Navbar from "./Navbar";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Autoplay, Pagination, Navigation } from "swiper";

export default function Hero() {
  return (
    <div className="text-white">
      <div
        className={` ${styles.bgHero}  w-full mx-auto text-center flex justify-center flex-col`}
      >
        <div>
          <Navbar />
        </div>
        <div className="h-[1300px]">
          <div className="flex flex-col p-10 mt-20">
            <div className="flex justify-center flex-col">
              <div className={styles.center}>
                <Swiper
                  spaceBetween={30}
                  autoplay={{
                    delay: 2500,
                    disableOnInteraction: false,
                  }}
                  modules={[Autoplay, Navigation]}
                  className="mySwiper"
                >
                  <SwiperSlide>
                    <img
                      className={`absolute bottom-0 ${styles.center}`}
                      src="https://www.mfec.co.th/wp-content/uploads/2022/07/Banner-Passion-1444x1536.jpg"
                      alt="image slide 1"
                    />
                  </SwiperSlide>
                  <SwiperSlide>
                    <img
                      className={styles.center}
                      src="https://www.mfec.co.th/wp-content/uploads/2022/07/Banner-Technology-1444x1536.jpg"
                      alt="image slide 2"
                    />
                  </SwiperSlide>
                </Swiper>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
