import React from "react";
import styles from "../styles/Home.module.css";

export default function Confident() {
  return (
    <main className={`p-10 ${styles.main}`}>
      <section className="flex justify-center flex-col items-center">
        <p className="text-4xl font-sans text-center text-blue-900 font-bold">
          HOW CONFIDENT ARE WE?
        </p>

        <div className="flex flex-row flex-wrap ">
          <div className="flex-initial w-64 px-10">
            <img
              src="/image/Counter_80.jpg"
              alt="test"
              width={501}
              height={501}
            />
            <div className="md:max-lg:flex">
              <div className={styles.h1}>
                <div className=" text-center text-blue-900 text-5xl font-bold">
                  68M+
                </div>
              </div>
              <div className="text-center">
                ผู้ใช้กว่า 68 ล้านคน ใช้บริการของ MFEC
              </div>
            </div>
          </div>

          <div className="flex-initial w-64 px-10">
            <img src="/image/Counter_400.jpg" alt="test" width={501} height={120} />
            <div className={styles.h1}>
              <div className=" text-center text-blue-900 text-5xl font-bold">
                400+
              </div>
            </div>
            <div className="text-center">
              MFEC มีผู้เชี่ยวชาญมากกว่า 400 ด้าน
            </div>
            <div className="text-center">ที่ได้รับการรับรองระดับโลก</div>
          </div>

          <div className="flex-initial w-64 px-10">
            <img src="/image/Counter_90.jpg" alt="test" width={501} height={120} />
            <div className={styles.h1}>
              <div className=" text-center text-blue-900 text-5xl font-bold">
                90
              </div>
            </div>
            <div className="text-center">
              คู่ค้าทางธุรกิจจากแบรนด์ เทคโนโลยีระดับโลก
            </div>
          </div>

          <div className="flex-initial w-64 px-10">
            <img src="/image/Counter_68M-1.jpg" alt="test" width={501} height={120} />
            <div className={styles.h1}>
              <div className=" text-center text-blue-900 text-5xl font-bold">
                80%
              </div>
            </div>
            <div className="text-center">ของลูกค้าอยู่ในตลาดหุ้น</div>
          </div>
        </div>
      </section>
    </main>
  );
}
