import React from "react";
import styles from "../styles/Home.module.css";
import Wave from "./Wave";
import { FaFacebook, FaPhoneVolume, FaPhoneAlt } from "react-icons/fa";
import { GrInstagram } from "react-icons/gr";
import { BsLinkedin, BsYoutube } from "react-icons/bs";

export default function Footer() {
  return (
    <section className={`${styles.footer} w-full`}>
      <Wave />
      <div className="flex flex-col">
        <div className={`flex flex-col ${styles.bg_footer}`}>
          <div className="flex justify-center flex-wrap items-center">
            <div className="flex flex-warp flex-col w-52 p-2.5">
              <div>
                <img
                  className="object-scale-down w-45 h-20"
                  src="https://www.mfec.co.th/wp-content/uploads/2022/05/Logo-MFEC-%E0%B8%AA%E0%B8%B5%E0%B8%82%E0%B8%B2%E0%B8%A7.png"
                  alt="MFEC"
                />
              </div>
              <div className="mt-3">
                <p>
                  MFEC PLC
                  ได้รับตำแหน่งเป็นผู้ให้บริการด้านไอทีชั้นนำที่เชี่ยวชาญด้านการประมวลผลเครือข่ายและธุรกิจอิเล็กทรอนิกส์
                </p>
              </div>
            </div>

            <div className="flex flex-col p-2.5">
              <div>
                <h3>About us</h3>
              </div>
              <div>
                <ul>
                  <li className={`${styles.menu_text} mt-3`}>
                    <a href="#">Company</a>
                  </li>
                  <div
                    className={`${styles.menu_text} flex flex-wrap flex-row`}
                  >
                    <div>
                      <a href="#">Careers</a>
                    </div>
                    <div className={`${styles.applyNow} rounded-full`}>
                      <a href="#">Apply Now !</a>
                    </div>
                  </div>
                  <div
                    className={`${styles.menu_text} flex flex-wrap flex-row`}
                  >
                    <div>
                      <a href="#">Our Service</a>
                    </div>
                    <div className={`${styles.Recommend} rounded-full`}>
                      <a href="#">Recommend</a>
                    </div>
                  </div>
                  <li className={styles.menu_text}>
                    <a href="#">Blog</a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="flex flex-col p-2.5">
              <div>
                <h3>For Customer</h3>
              </div>
              <div>
                <ul className={`${styles.menu_text} mt-3`}>
                  <li>
                    <a href="#">Contact Us</a>
                  </li>
                  <li>
                    <a href="#">Cookies Policy</a>
                  </li>
                  <li>
                    <a href="#">Customer Privacy Notice</a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="flex flex-col p-2.5">
              <div>
                <h3>For Partner</h3>
              </div>
              <div>
                <ul className={`${styles.menu_text} mt-3`}>
                  <li>
                    <a href="#">Become Partner</a>
                  </li>
                  <li>
                    <a href="#">Vendor Privacy Notice</a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="flex flex-col p-2.5">
              <div>
                <h3>Corporate Identity</h3>
              </div>
              <div>
                <ul className={`${styles.menu_text} mt-3`}>
                  <li>
                    <a href="#">Brand Guideline</a>
                  </li>
                  <li>
                    <a href="#">Logo Download</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="flex justify-center flex-row flex-wrap items-center">
            <div className="flex flex-wrap flex-row">
              <div className="flex">
                <div className="flex p-2.5">
                  <div className="flex flex-row">
                    <div>
                      <FaPhoneAlt className={styles.icon_phone}></FaPhoneAlt>
                    </div>
                    <div>
                      <div>
                        <span className={styles.textSpan}>
                          Contact for information
                        </span>
                      </div>
                      <div className="mt-4">
                        <p>02 - 821 - 7999</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="flex">
                <div className="flex p-2.5">
                  <div className="flex flex-warp flex-row">
                    <div>
                      <FaPhoneVolume
                        className={styles.icon_phone1}
                      ></FaPhoneVolume>
                    </div>
                    <div>
                      {" "}
                      <div>
                        <span className={styles.textSpan}>
                          Contact Helpdesk for Support
                        </span>
                      </div>
                      <div className="mt-4">
                        <p>02 - 821 - 7999</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="flex">
                <div className="flex p-2.5">
                  <ul className={styles.social_icon}>
                    <li>
                      <a href="#">
                        <FaFacebook className={styles.icon}></FaFacebook>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <BsLinkedin className={styles.icon}></BsLinkedin>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <GrInstagram className={styles.icon}></GrInstagram>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <BsYoutube className={styles.icon}></BsYoutube>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="flex justify-center flex-row w-full">
              <div className="flex flex-col p-5">
                <span className={styles.line}></span>
              </div>
            </div>
            <div className="flex flex-row">
              <div>
                <p>MFEC Public Company Limited © 2022</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
