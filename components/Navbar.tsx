import React from "react";
import { RiMenu2Line } from "react-icons/ri";
import styles from "../styles/Home.module.css";


export default function Navbar() {
  return (
    <nav>
      <div className="flex justify-between items-center h-24  mx-auto px-4 text-white">
        <div>
        <img
          className="w-24 h-14"
          src="https://www.mfec.co.th/wp-content/uploads/2022/05/Logo-MFEC-%E0%B8%AA%E0%B8%B5%E0%B8%82%E0%B8%B2%E0%B8%A7.png"
          alt="LOGO_MFEC"
        />
        </div>
        <div className={styles.textNav}>
               <ul className="flex">
               <li className="p-4">Home</li>
               <li className="p-4">About</li>
               <li className="p-4">Solutions & Service</li>
               <li className="p-4">Partner</li>
               <li className="p-4">Industry</li>
               <li className="p-4">IR</li>
               <li className="p-4">Career</li>
               <li className="p-4">Blog</li>
             </ul>
        </div>
 

        <div className="flex">
          <a href="" className={`${styles.buttonColor1} rounded-full mt-3 mr-5`}>Contact Us</a>
          <p className={`${styles.textNav} p-4`}>ไทย</p>
        </div>

        <div className="flex">
          <RiMenu2Line size={20} p-4 />
        </div>
      </div>
    </nav>
  );
}
