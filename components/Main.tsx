import React from 'react'
import styles from "../styles/Home.module.css";

export default function Main() {
 return (
  <section className={`pt-20 pb-20 flex flex-warp justify-center ${styles.div_background}`}>
  <div className="flex flex-col flex-wrap">
    <div className="flex flex-col flex-warp  p-5">
      <div className={styles.textTitle}>Customer and Partner </div>
      <div className={styles.textMain}>
        <p>Ready to uplift your digital life?</p>
      </div>
    </div>

    <div className="flex justify-center flex-row mr-20">
      <div className="flex flex-col p-5">
        <a href="#" className={`${styles.card} shadow-2xl`}>
          <div className="flex flex-row">
            <span>
              <img
                className={styles.imgCard}
                src="https://www.mfec.co.th/wp-content/uploads/2022/06/Home_For-Customer-1.png"
                alt="MFEC"
              />
            </span>
            <div className="flex justify-center ml-10 flex-col">
              <div className={styles.textCard}>
                <h3>For Customer</h3>
                <h3 className={styles.textSlide}>&gt;</h3>
              </div>
              <div className={`${styles.buttonColor} rounded-md mt-3`}>
                <p>Click</p>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div className="flex flex-col p-5">
        <a href="#" className={`${styles.card} shadow-2xl`}>
          <div className="flex flex-row">
            <span>
              <img
                className={styles.imgCard}
                src="https://www.mfec.co.th/wp-content/uploads/2022/06/Home_For-Partner-1.png"
                alt="MFEC"
              />
            </span>
            <div className="flex justify-center ml-10 flex-col">
              <div className={styles.textCard}>
                <h3>For Partner</h3>
                <h3 className={styles.textSlide1}>&gt;</h3>
              </div>
              <div className={`${styles.buttonColor} rounded-md mt-3`}>
                <p>Click</p>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>
</section>
  )
}
